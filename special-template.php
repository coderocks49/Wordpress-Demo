<!--template layout to be used by multiple pages-->

<?php

//This template name 'Special Layout' will be seen as Option for the select of Template
//in page attribute section while creating page from the dashboard

/*
 Template Name: Special Layout
 */

//importing header
get_header();

//Loop through all Posts
if (have_posts()):
    while (have_posts()): the_post(); ?>
        <article class="post page" style="min-height: 400px; max-height: auto;">
            <h2><?php the_title(); ?></h2>

            <!-- info-box -->
            <div class="info-box" style="width: 30%;  float: right;  margin: 0 0 30px 30px;  padding: 20px;  background-color: #eee;  outline: 1px solid black;">
                <h4 style="margin-bottom: 6px;">Disclaimer Title</h4>
                <p style="font-size: 85%;">probo discere no pro. Utinam quaestio eum te. Idque dissentias ex mei, has quaestio omittantur vituperatoribus eu, sed ne dolorum quaestio. At qui audire persius, tibique recusabo ius id, et eripuit singulis eam. Audiam utroque officiis his at.Ei pri maiorum minimum, dicat iracundia mnesarchum vel ex Cu his recusabo platonem theophrastus. Sed natum appellantur ut, eu cibo ridens sententiae eos.Ei vidit quaestio vim, eu sit volumus vituperata interpretaris. Mei id doming volumus recteque. </p>
            </div><!-- end of info-box -->

            <?php the_content(); ?>
        </article>
    <?php endwhile;

else:
    echo '<p>No Content Found</p>';
endif;

get_footer();
?>
