<?php

//function import default stylesheet
function learningWordPress_resources()
{
    wp_enqueue_style('style', get_stylesheet_uri());
}

//add function to run it
add_action('wp_enqueue_scripts', 'learningWordPress_resources');

//Get top ancestor
function get_top_ancestor_id()
{
    global $post;
    if ($post->post_parent) {
        $ancestors = array_reverse(get_post_ancestors($post->ID));
        return $ancestors[0];
    }
    return $post->ID;
}

//Does page have children
function has_children()
{
    global $post;
    //get children of currently viewing page
    $pages = get_pages('child_of=' . $post->ID);
    return count($pages);
}

//Customize excerpt word count length
function custom_excerpt_length()
{
    return 25;
}

add_filter('excerpt_length', 'custom_excerpt_length');


function learningWordPress_setup()
{
    //Navigation menu registration to be used in our theme
    register_nav_menus([
        'primary' => __('Primary Menu'), //'primary' that is defined in header section as menu location
        // and gave alias as 'Primary Menu' which will be seen
        // as category while managing menus in dashboard
        'footer' => __('Footer Menu') //'footer' that is defined in footer section as menu location
        // and gave alias as 'Primary Menu' which will be seen
        // as category while managing menus in dashboard
    ]);
    //Add featured image support
    add_theme_support('post-thumbnails');
    add_image_size('small-thumbnail', 180, 120, true);
    add_image_size('banner-image', 920, 210, array('left', 'top')); //defined what portion to be cropped of image in array
}

//function run automatically after the theme setup
add_action('after_setup_theme', 'learningWordPress_setup');