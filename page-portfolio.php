<?php
//importing header
get_header();

//Loop through all Posts
if (have_posts()):
    while (have_posts()): the_post(); ?>
        <article class="post page">
            <!-- column-container-->
            <div class="column-container clearfix">
                <div class="title-column">
                    <h2><?php the_title();?></h2>
                </div>
                <div class="text-column">
                    <?php the_content(); ?>
                </div>
            </div><!--end of column-container-->
        </article>
    <?php endwhile;

else:
    echo '<p>No Content Found</p>';
endif;

get_footer();
?>
