<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<!--defining which language to be used dynamically-->
<html <?php language_attributes() ?>>
<head>
    <!-- Defining Meta charset -->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <!-- Defining title dynamically-->
    <title><?php bloginfo('name'); ?></title>
    <!-- function for wordpress to add any changes or content in head by itself-->
    <?php wp_head(); ?>
</head>
<!--adding body_class to target different pages with their css-->
<body <?php body_class(); ?>>
<div class="container">
    <!--site-header-->
    <header class="site-header">
        <!-- output the site name and also adding link for navigating to home when clicked-->
        <h1><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></h1>
        <!-- output the description-->
        <h5><?php bloginfo('description'); ?><?php if(is_page('portfolio')){?>
                - Thank you for viewing our work
            <?php }?></h5>
        <!-- add navigation-->
        <nav class="site-nav">
            <?php
            $args = [
                'theme_location' => 'primary' //for defining location of the menu
            ];
            ?>
            <?php wp_nav_menu($args); ?>
        </nav>
    </header>
    <!--end of site header-->