<!--Page to show the archive heading by author or category or tags or months or blah blah blah-->

<?php
//importing header
get_header();

//Loop through all Posts
if (have_posts()):
    ?>

    <!--Archive heading-->
    <h2><?php
        if (is_category()) {
            single_cat_title();
        } else if (is_tag()) {
            single_tag_title();
        } else if (is_author()) {
            the_post();
            echo 'Author Archives: ' . get_the_author();
            rewind_posts();
        } else if (is_day()) {
            echo 'Daily Archives: ' . get_the_date();
        } else if (is_month()) {
            echo 'Monthly Archives: ' . get_the_date('F Y');
        } else if (is_year()) {
            echo 'Yearly Archives: '.get_the_date('Y');
        } else {
            echo 'Archives:';
        }
        ?></h2>
    <!-- /Archive heading-->

    <?php
    while (have_posts()): the_post(); ?>
        <article class="post">
            <!-- Adding link as permalink, so that whenever
                the title is clicked, it navigates that single
                post itself -->
            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <p class="post-info"><?php the_time('F j, Y g:i a'); ?> | by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a> | Posted in
                <?php
                $categories = get_the_category();
                $separator = ", ";
                $output = "";

                if ($categories) {
                    foreach ($categories as $category) {
                        $output .= '<a href="' . get_category_link($category->term_id) . '">' . $category->cat_name . '</a>' . $separator;
                    }
                    echo trim($output, $separator);
                }
                ?>
            </p>
            <?php the_excerpt(); ?>
        </article>
    <?php endwhile;

else:
    echo '<p>No Content Found</p>';
endif;

get_footer();
?>
