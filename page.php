<?php
//importing header
get_header();

//Loop through all Posts
if (have_posts()):
    while (have_posts()): the_post(); ?>
        <article class="post page">
            <?Php
            if (has_children() OR $post->post_parent > 0) {
                ?>
                <nav class="site-nav children-links clearfix">
                    <!-- to show the links of the top most parent page-->
                    <span class="parent-link"><a href="<?php echo get_the_permalink(get_top_ancestor_id()) ?>"><?php echo get_the_title(get_top_ancestor_id()) ?></a></span>
                    <!-- /to show the links of the top most parent page-->
                    <ul>
                        <!--to show the links of the children pages as nav-->
                        <?php
                        $args = [
                            'child_of' => get_top_ancestor_id(), //function function define on functions.php
                            'title_li' => ''
                        ];
                        ?>
                        <?php wp_list_pages($args); ?>
                        <!-- /end to show the links of the children pages as nav-->
                    </ul>
                </nav>
            <?php } ?>
            <h2><?php the_title(); ?></h2>
            <?php the_content(); ?>
        </article>
    <?php endwhile;

else:
    echo '<p>No Content Found</p>';
endif;

get_footer();
?>
