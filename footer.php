<footer class="site-footer">
    <!-- add naviagtion to footer-->
    <nav class="site-nav">
        <?php
        $args = [
            'theme_location' => 'footer' //for defining location of the menu
        ];
        ?>
        <?php wp_nav_menu($args); ?>
    </nav>
    <!-- output name of the site, copyright symbol and the year-->
    <p><?php bloginfo('name'); ?> - &copy; <?php echo date('Y'); ?></p>
</footer>
</div><!-- end of container div-->
<!-- function for wordpress to add any changes or content in footer by itself-->
<?php wp_footer(); ?>
</body>
</html>